# Synchronization of Continuous Time and Discrete Events Simulation in SystemC 

## Description:
This repository contains an implementation of the algorithms and case studies described in the paper *Synchronization of Continuous Time and Discrete Events Simulation in SystemC*, submitted to the TCAD journal.
It contains, in particular, the source code of:
* The continuous time and discrete events synchronization algorithm.
* The following three case studies: 
    1. Vehicle's Automatic Transmission Control 
    2. Bouncing Ball
    3. Switched RC Circuit

The synchronization algorithm is located in the `sync_alg` folder.
It is implemented  by the `synchronization_layer` class.
Apart from this class, a set of other classes build up the infrastructure for modeling and simulation of CT/DE systems that we use in the case studies: 
* `numerical_solver`: class implementing the integration and root location method.
* `ode_system`: abstract class that declares common functions to the CT modules, such as Get-Derivatives, Execute-Updates, Is-Event and Generate-Outputs. 
* `ct_state`: class that eases the manipulation of the CT state.
* `inputs_manager`: class that manages input events to the CT modules. 
* `ct_module`: class that links the SystemC `sc_module` class to the `ode_system` class and that seves as a base class for CT modules. 

Please note that this implementation is a proof of concept and it is neither intended to serve as a reference implementation nor it is suitable for direct use in industrial applications. Its only aim is to support the claim that the algorithms described in our paper are feasible.

The three case studies are located in the `case_studies` folder. 
For each case study, we provide our model in the `ct_de` folder, the Timed Dataflow Model in the `tdf` folder, and the MATLAB/Simulink reference model in the `matlab` folder. For compilation and execution of these case studies, see the instructions below.


## Dependencies:
1. [SystemC 2.3.1a.](https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.1a.tar.gz)
2. Boost libraries, to install on debian-based systems execute `sudo apt-get install libboost-all-dev`
3. (Optional, for comparison purposes) [SystemC AMS](https://www.coseda-tech.com/systemc-ams-proof-of-concept) compiled against SystemC 2.3.1a. 
4. (Optional, for comparison purposes) MATLAB/Simulink.
4. (Optional) GNU Octave.


## Execution:

In the `ct_de` and `tdf` folders of each case study:

1. In the `Makefile`, set the SYSTEMC_PREFIX and SYSTEMC_AMS_PREFIX environment variables with the SystemC and SystemC AMS installation paths.

2. Execute `make` to compile:
```console
foo@bar:~$ make 
```

3. Simulate:
```console
foo@bar:~$ ./main
```

4. (Optional) Run the Octave script to display the results:
```console
foo@bar:~$ octave results.m 
```

For the MATLAB/Simulink version of the Vehicle's Automatic Transmission Control case study, the script `case_studies/vehicle_cruise_control/matlab/parameter_values.m` has to be run before executing the Simulink model. Its purpose is to load the model's parameter values. 