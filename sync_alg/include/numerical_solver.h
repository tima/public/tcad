#ifndef NUMERICAL_SOLVER_H
#define NUMERICAL_SOLVER_H

#include <utility>
#include <algorithm>
#include <array>

///////////////////////////////////////////////////////////////////////
// SOLVER LIBRARY
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
#include <boost/numeric/odeint/stepper/generation.hpp>
#include <boost/numeric/odeint/iterator/adaptive_time_iterator.hpp>
#include <boost/numeric/odeint/iterator/adaptive_iterator.hpp>
// #include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
// #include <boost/numeric/odeint/stepper/dense_output_runge_kutta.hpp>
namespace odeint = boost::numeric::odeint;
///////////////////////////////////////////////////////////////////////


// Integration algorithm is adaptive, 
// these values refer just to the first step
#define MAX_STEP 0.1 // seconds
#define MIN_STEP 0.01 // seconds
 

namespace sct_kernel {

    class numerical_solver {
        public: 
            numerical_solver(){};

            // Integration method
            template<class DiffEquations, class State, class IntegrationObserver>
            void integrate( DiffEquations diff_equations, State &state, 
                double t_start, double t_end, IntegrationObserver observer)
            {
                double int_step = (t_end-t_start)/ 10.0;
                int_step = int_step > MAX_STEP ? MAX_STEP : int_step; 
                int_step = int_step < MIN_STEP ? MIN_STEP : int_step; 
                
                typedef odeint::controlled_runge_kutta< odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra> > stepper_type;

                auto stepper = stepper_type();

                int steps = odeint::integrate_adaptive( stepper, diff_equations , state , t_start , t_end , int_step, observer);
                std::cout << "STEPS " << steps << std::endl;
            }  


            // Root location method
            template<class DiffEquations, class State, class StateEventDetector, class Tracer>
            bool locate_state_event(
                DiffEquations diff_equations, const State &x0, State &x_at_event,
                const double t_start, double &t_end,
                StateEventDetector is_event, 
                Tracer tracer) 
            {
                const double precision = 1E-6;
                
                // integrates an ODE until some threshold is crossed
                // returns time and state at the point of the threshold crossing
                // if no threshold crossing is found, some time > t_end is returned
   
                auto stepper = odeint::make_dense_output( 1.0e-4, 1.0e-4, odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra>() );

                //  odeint::integrate_adaptive( stepper, diff_equations , state , t_start , t_end , int_step, is_event);

                if (!validate_interval(stepper, diff_equations, is_event, tracer, x0, t_start, t_end)) {
                    #ifdef DEBUG
                        std::cout << "INVALID INTERVAL " << std::endl;
                    #endif

                    return false;
                }

                // the dense out stepper now covers the interval where the condition changes
                // improve the solution by bisection
                double t0 = stepper.previous_time();
                double t1 = stepper.current_time();


                double t_m = 0.5 * (t0 + t1);
                State x_m(x0.size());
                
                // TODO: Make sure to return exactly the point where the state event occurs
                while (std::abs(t0 - t_m) > precision/2.0) {
                    stepper.calc_state(t_m, x_m); // obtain the corresponding state
                    if (is_event(x_m, t_m))
                        t1 = t_m;  // condition changer lies before midpoint
                    else
                        t0 = t_m;  // condition changer lies after midpoint
                    t_m = 0.5 * (t0 + t1);  // get the mid point time
                }

                // Make sure to use the exact time where the threshold 
                // event occurs
                t_m = t1;

                // we find the interval of size eps
                stepper.calc_state(t_m, x_m);

                t_end = t_m;
                x_at_event = x_m;

                return true;
            }

        private:

            template<class Stepper, class DiffEquations, class State, class StateEventDetector, class Tracer> 
            bool validate_interval( Stepper &stepper,
                DiffEquations diff_equations,
                StateEventDetector is_event, Tracer tracer, State x0, 
                const double t_start, double &t_end )
            {
                double dt;
                double int_step = (t_end-t_start)/ 10.0;
                int_step = int_step > MAX_STEP ? MAX_STEP : int_step; 
                int_step = int_step < MIN_STEP ? MIN_STEP : int_step; 

                dt = int_step;

                auto ode_range = odeint::make_adaptive_time_range(std::ref(stepper), 
                    diff_equations,
                    x0,
                    t_start,
                    t_end,
                    dt
                );

                auto it_start = ode_range.first;
                // Avoid trying to locate threshold in an invalid interval
                State x_1 = (*it_start).first;
                double t_1 = (*it_start).second;
                if (is_event(x_1, t_1)) {
                    #ifdef DEBUG
                        std::cout << "INVALID INTERVAL : START" <<
				(*it_start).first[0]     << std::endl;
                    #endif

                    return  false;
                }

                auto found_iter = std::find_if(ode_range.first,
                ode_range.second,
                [&](const std::pair<State const&, double> &it){ 
                    State x = it.first; 
                    double t = it.second;
                    tracer(t, x);
                    return is_event(x, t);
                });    

                #ifdef DEBUG
                    std::cout << "Finding between " << t_start << " and " << t_end << std::endl;
                #endif

                auto it_end = ode_range.second;
                State x_2 = (*it_end).first; 
                double t_2 = (*it_end).second;
                if (found_iter == it_end && !is_event(x_2, t_2)) {
                    #ifdef DEBUG
                        std::cout << "INVALID INTERVAL : REACHED END WITHOUT FINDING EVENT" << std::endl;
                    #endif
                    return  false;
                }

                return true;
            }

    };

}

#endif


// #ifndef NUMERICAL_SOLVER_H
// #define NUMERICAL_SOLVER_H

// #include <utility>
// #include <algorithm>
// #include <array>

// ///////////////////////////////////////////////////////////////////////
// // SOLVER LIBRARY
// #include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
// #include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
// #include <boost/numeric/odeint/stepper/generation.hpp>
// #include <boost/numeric/odeint/iterator/adaptive_iterator.hpp>
// namespace odeint = boost::numeric::odeint;
// ///////////////////////////////////////////////////////////////////////


// // Integration algorithm is adaptive, 
// // these values refer just to the first step
// #define MAX_STEP 0.1 // seconds
// #define MIN_STEP 0.01 // seconds


// namespace sct_kernel {

//     class numerical_solver {
//         public: 
//             numerical_solver(){};

//             // Integration method
//             template<class DiffEquations, class State, class IntegrationObserver>
//             void integrate( DiffEquations diff_equations, State &state, 
//                 double t_start, double t_end, IntegrationObserver observer)
//             {
//                 double int_step = (t_end-t_start)/ 10.0;
//                 int_step = int_step > MAX_STEP ? MAX_STEP : int_step; 
//                 int_step = int_step < MIN_STEP ? MIN_STEP : int_step; 
                
//                 std::cout << "INTEGRATIB " << int_step << std::endl;

//                 // auto stepper = odeint::make_dense_output( 1.0e-6, 1.0e-6, odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra>() );

//                 // // typedef odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra> stepper_type;

//                 // // auto stepper = odeint::make_controlled<stepper_type>(1E-6 , 1E-6);

//                 typedef odeint::controlled_runge_kutta< odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra> > stepper_type;


//                 //                 auto stepper = stepper_type();

//                 //                  odeint::integrate_adaptive( stepper, diff_equations , state , t_start , t_end , int_step, observer);
//                 //             }


//                  odeint::integrate_adaptive( stepper_type(), diff_equations , state , t_start , t_end , int_step, observer);
//             }


//             // Root location method
//             template<class DiffEquations, class State, class StateEventDetector>
//             bool locate_state_event(
//                 DiffEquations diff_equations, const State &x0, State &x_at_event,
//                 const double t_start, double &t_end,
//                 StateEventDetector is_event) 
//             {
//                 const double precision = 1E-6;
                
//                 // integrates an ODE until some threshold is crossed
//                 // returns time and state at the point of the threshold crossing
//                 // if no threshold crossing is found, some time > t_end is returned

//                  auto stepper = odeint::make_dense_output( 1.0e-6 , 1.0e-6 , odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra>() );


//                 if (!validate_interval(stepper, diff_equations, is_event, x0, t_start, t_end)) {
//                     #ifdef DEBUG
//                         std::cout << "INVALID INTERVAL " << std::endl;
//                     #endif

//                     return false;
//                 }

//                 // the dense out stepper now covers the interval where the condition changes
//                 // improve the solution by bisection
//                 double t0 = stepper.previous_time();
//                 double t1 = stepper.current_time();


//                 double t_m = 0.5 * (t0 + t1);
//                 State x_m(x0.size());
                
//                 // TODO: Make sure to return exactly the point where the state event occurs
//                 while (std::abs(t0 - t_m) > precision/2.0) {
//                     stepper.calc_state(t_m, x_m); // obtain the corresponding state
//                     if (is_event(x_m))
//                         t1 = t_m;  // condition changer lies before midpoint
//                     else
//                         t0 = t_m;  // condition changer lies after midpoint
//                     t_m = 0.5 * (t0 + t1);  // get the mid point time
//                 }

//                 // Make sure to use the exact time where the threshold 
//                 // event occurs
//                 t_m = t1;

//                 // we find the interval of size eps
//                 stepper.calc_state(t_m, x_m);

//                 t_end = t_m;
//                 x_at_event = x_m;

//                 return true;
//             }

//         private:

//             template<class Stepper, class DiffEquations, class State, class StateEventDetector> 
//             bool validate_interval( Stepper &stepper,
//                 DiffEquations diff_equations,
//                 StateEventDetector is_event, State x0, 
//                 const double t_start, double &t_end )
//             {
//                 double dt = (t_end - t_start) / 100.0;
//                 dt = MAX_STEP; // / 100; // < 1E-04 ? 1E-04 : dt; 
                

//                 auto ode_range = odeint::make_adaptive_range(std::ref(stepper), 
//                     diff_equations,
//                     x0,
//                     t_start,
//                     t_end,
//                     dt
//                 );

//                 // Avoid trying to locate threshold in an invalid interval
//                 if (is_event(*ode_range.first, 5)) {
//                     #ifdef DEBUG
//                         std::cout << "INVALID INTERVAL : START" << (*ode_range.first)[0]     << std::endl;
//                     #endif

//                     return  false;
//                 }

//                 auto found_iter = std::find_if(ode_range.first, ode_range.second, is_event);    


//                 #ifdef DEBUG
//                     std::cout << "Finding between " << t_start << " and " << t_end << std::endl;
//                 #endif

//                 if (found_iter == ode_range.second && !is_event(*ode_range.second)) {
//                     #ifdef DEBUG
//                         std::cout << "INVALID INTERVAL : REACHED END WITHOUT FINDING EVENT" << std::endl;
//                     #endif
//                     return  false;
//                 }

//                 return true;
//             }

//     };

// }

// #endif