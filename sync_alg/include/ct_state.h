#ifndef CT_STATE_H
#define CT_STATE_H

#include <algorithm>
#include <vector>
#include <set>
#include <boost/operators.hpp>
#include <boost/numeric/odeint.hpp>


// CT class based on examples from 
// https://www.boost.org/doc/libs/1_62_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/odeint_in_detail/state_types__algebras_and_operations.html

namespace sct_core{
    
    class my_vector : 
        boost::additive1< my_vector ,
        boost::additive2< my_vector , double ,
        boost::multiplicative2< my_vector , double > > >
    {
        typedef std::vector< double > vector;

    public:
        typedef vector::iterator iterator;
        typedef vector::const_iterator const_iterator;

    public:
        my_vector( const size_t N );

        my_vector();

        const double & operator[]( const size_t n ) const;

        double & operator[]( const size_t n );

        iterator begin();
        const_iterator begin() const;

        iterator end();
        
        const_iterator end() const;

        size_t size() const;

        void resize( const size_t n );

        my_vector& operator+=( const my_vector &p );

        my_vector& operator*=( const double factor );

    private:
        std::vector< double > m_v;

        // Utility 
        void init_after_resize(int from);
    };

    my_vector operator/( const my_vector &p1 , const my_vector &p2 );

    my_vector abs( const my_vector &p );

    typedef my_vector ct_state;
}

 namespace boost { namespace numeric { namespace odeint {

    template<>
    struct is_resizeable< sct_core::my_vector >
    {
        typedef boost::true_type type;
        static const bool value = type::value;
    };

    } } }


    namespace boost { namespace numeric { namespace odeint {

    template<>
    struct vector_space_norm_inf< sct_core::my_vector >
    {
        static double abs_compare(double a, double b)
        {
            return (std::fabs(a) < std::fabs(b)) ? std::fabs(b) : std::fabs(a);
        }
        
        typedef double result_type;
        double operator()( const sct_core::my_vector &v ) const
        {
            double max = *v.begin();
            for( auto el: v ){
                if( std::fabs(el) > max){
                    max = std::fabs(el);
                }
            } 
            return max;
        }
    };

    } } }
    
#endif