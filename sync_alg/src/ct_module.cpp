#include "../include/ct_module.h"

sct_core::ct_module::ct_module(){

}

void sct_core::ct_module::end_of_elaboration(){
    // Invoke method to set attributes as defined by the user
    set_sync_parameters();

    // Register input ports in inputs manager
    sct_kernel::ode_system::inputs.register_input_ports(sc_core::sc_module::get_child_objects());

    // Create synchronization layer object
    this->sync_layer = new sct_kernel::synchronization_layer(this, max_step);

    sync_layer->store_input_event_sensitivity_list(
        sct_kernel::ode_system::inputs.get_sensitivity_list()
    );


    // Set initial conditions (as defined by the user)
    // and create a checkpoint on them
    set_initial_conditions();
    sct_kernel::ode_system::backup_state();
}

// To be implemented by the user, 
// as in TDF modules of SystemC AMS
// e.g. call set_max_timestep
// The default behavior is to set 
// a default maximum timestep
void sct_core::ct_module::set_sync_parameters() {
    // Set default maximum synchronization timestep
    set_max_timestep(DEFAULT_DELTA_T);
}

// Set the maximum allowed synchronization timestep
void sct_core::ct_module::set_max_timestep(double max){
    std::cout << "Setting attribute " << max << std::endl;
    if(max > MIN_DELTA_T){
        max_step = max;
    }
    else{
        max_step = MIN_DELTA_T;
        std::cout << "Couldn't set DELTA T below " << max_step << " s. Set to " << max_step << std::endl; 
    }
}

void sct_core::ct_module::set_initial_conditions() {
    for (auto&& x_val : sct_kernel::ode_system::x) {
        x_val = 0;
    }
}

// 
void sct_core::ct_module::end_of_simulation() {
    std::cout << "************ STATISTICS ***************** " << std::endl;
    std::cout << "# of backtracks : " << sync_layer->get_backtrack_count() << std::endl;

    create_trace_file(name());
}