#include "../include/ct_state.h"


std::ostream& operator<<( std::ostream &out , const sct_core::my_vector &p )
{
    for (auto it: p) {
        out << it << " ";   
    }
    return out;
}

sct_core::my_vector::my_vector( const size_t N )
    : m_v( N )
{
    m_v.resize( N );
    init_after_resize(N);
}

sct_core::my_vector::my_vector()
    : m_v()
{
    int N = 3; 
    m_v.resize(N);
    init_after_resize(N);
}

const double &  sct_core::my_vector::operator[]( const size_t n ) const {
    return m_v[n];
}

double &  sct_core::my_vector::operator[]( const size_t n ) { 
    if( (n+1) > this->size() ){
        resize(n+1);
    }

    return m_v[n]; 
}

sct_core::my_vector::iterator  sct_core::my_vector::begin() { 
    return m_v.begin();
}

sct_core::my_vector::const_iterator  sct_core::my_vector::begin() const {
    return m_v.begin(); 
}

sct_core::my_vector::iterator  sct_core::my_vector::end() { 
    return m_v.end(); 
}

sct_core::my_vector::const_iterator sct_core::my_vector::end() const { 
    return m_v.end(); 
}

size_t  sct_core::my_vector::size() const { 
    return m_v.size(); 
}

void  sct_core::my_vector::resize( const size_t n ) {
    int last_el = size();
    m_v.resize( n );    
    init_after_resize(last_el);
}

sct_core::my_vector&  sct_core::my_vector::operator+=( const my_vector &p ) {
    if(p.size() == 0){
        return *this;
    }
          
    if (p.size() != size()) {
        p.resize(size());
//        throw std::invalid_argument("cannot add states of different sizes");
    }

    // std::cout << "ADDING " << *this << " and " << p << std::endl; 

    // Add this vector to the input vector
    // Isn't it elegant ? Thanks stackoverflow. 
    std::transform(this->begin(), this->end(), p.begin(), this->begin(), std::plus<double>());

    return *this;
}

sct_core::my_vector&  sct_core::my_vector::operator*=( const double factor )
{
    std::transform(this->begin(), this->end(), this->begin(), 
        [factor](double el) -> double { return el*factor; });
    return *this;
}


// Utility 
void  sct_core::my_vector::init_after_resize(int from){
    for( size_t i = from; i < this->size(); i++ ){
        m_v[i] = 0.0;
    }
}


sct_core::my_vector sct_core::operator/( const sct_core::my_vector &p1 , const sct_core::my_vector &p2 )
{
    sct_core::my_vector result(p1.size());
    std::transform(p1.begin(), p1.end(), p2.begin(), result.begin(), std::divides<double>());

    return result;
}

sct_core::my_vector sct_core::abs( const sct_core::my_vector &p )
{
    sct_core::my_vector result(p.size());
    float (*fabs)(float) = &std::fabs; 

    std::transform(p.begin(), p.end(), result.begin(), 
            fabs);

    return result;
}


