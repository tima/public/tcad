#include<systemc>
#include<systemc-ams>
#include<iostream> 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "switch_driver_de.h"
#include "circuit_sc_module.h"

int sc_main(int argc, char* argv[]){
    clock_t t = clock();
  
    sc_core::sc_signal<double> v_sg;
    sc_core::sc_signal<bool> switch_sg, up_sg, down_sg;
    sc_core::sc_event_queue ev_q;

    SwitchDriverDE switch_driver("driver");
    CircuitScModule circuit("circuit");

    switch_driver.switch_out.bind(switch_sg);
    switch_driver.up_in.bind(up_sg);
    switch_driver.down_in.bind(down_sg);

    circuit.v_out.bind(v_sg);
    circuit.switch_in.bind(switch_sg);
    circuit.up_out.bind(up_sg);
    circuit.down_out.bind(down_sg);

    sca_util::sca_trace_file *atf;
    atf = sca_util::sca_create_tabular_trace_file("v_out.dat");
    sca_trace(atf, v_sg, "vc");
    sca_trace(atf, switch_sg, "switch");

    // Start simulation for a given time
    sc_core::sc_start(2, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();

    sca_util::sca_close_tabular_trace_file(atf);

    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n", (int) t,((float)t)/CLOCKS_PER_SEC);

    return 0;
}