#include <iostream>
#include <fstream>
#include <string>
#include <systemc>
#include <systemc-ams> /* Trace utilities */ 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "vehicle_ct.h"
#include "transmission_control_unit.h"


int sc_main(int argc, char *argv[]){
    clock_t t = clock();

    VehicleCt vehicle_ct("vehicle_ct");
    TransmissionControlUnit tcu("tcu");


    sc_core::sc_signal<double> gear_sig("gear_sig"), speed_sig("speed");
    sc_core::sc_signal<double> gear_ratio_sig("gear_ratio_sig");
    sc_core::sc_signal<ShiftingThresholds> threshold_sig("threshold_sig");
    sc_core::sc_event_queue up_evq("up_evq");

    vehicle_ct.update_out(up_evq);
    vehicle_ct.threshold_out(threshold_sig);
    vehicle_ct.gear_in(gear_sig);
    vehicle_ct.gear_ratio_in(gear_ratio_sig);
    vehicle_ct.speed_out(speed_sig);

    tcu.update_in(up_evq);
    tcu.threshold_in(threshold_sig);
    tcu.gear_out(gear_sig);
    tcu.gear_ratio_out(gear_ratio_sig);


    sca_util::sca_trace_file *atf;
    atf = sca_util::sca_create_tabular_trace_file("speed.dat");
    sca_trace(atf, speed_sig, "v");
    sca_trace(atf, gear_sig, "gear");
    sca_trace(atf, threshold_sig, "gear");


    sc_start(30, sc_core::SC_SEC);
    sc_core::sc_stop();

    sca_util::sca_close_tabular_trace_file(atf);

    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);

    return 0;
}