delta_t = [0.01	0.1	1	2	2.5	3	5	10	100];

wc_time = [0.04842683	0.01359583	0.01141650	0.01025633	0.01083917	0.01120533	0.01124083	0.01131050	0.01215667];

wc_adaptive = 0.009754555556

figure(2);
hold on;
grid on;
loglog(delta_t, wc_time, '-bo', 'LineWidth', 2);
loglog([0.01 10], [wc_adaptive wc_adaptive], '-r', 'LineWidth', 2);
ytick = [0.04842683	0.01359583		0.01025633 0.009754555556 		0.01215667];
xtick = [0.01	0.1 0.29	2	20	100];
set(gca,'YTick', ytick);
set(gca,'XTick', xtick);
ylim([ 0.0094 0.049]);
legend("Wall-clock time vs Dt", "Wall-clock time adaptive Dt")

input("Press enter");